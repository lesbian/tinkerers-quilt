<!--suppress HtmlDeprecatedTag, XmlDeprecatedElement -->
<center><img alt="Tinkerer's Quilt banner" src="https://cdn.modrinth.com/data/vrkQdo9Y/images/8088722fdded8e265ca038739b4a5605c6dfb1d4.png" />
A failure in minimalism - and a modpack with three mods.<br/>
Making minecraft smoother, more discoverable, and show you cool mods since 2022.<br/>
</center>


---

### Alpha Note

TQuilt Modded is in alpha! It should be stable, but the quest modventuring quest trees are blatantly unfinished. Try one of the other two packs for a more complete experience!

---

# Pack Features

## Celebrity Celebration [Major Mods]

Tinkerer's Quilt Modded offers an easy inroad to explore polished, interesting mods with their own progression and gameplay systems.

These mods are elaborate, and come equipped with quest trees that mimic their various facets of progression.

This places the total quest count at 2##.

| Relevant Mod                                     | Summary                                                                                  |
|--------------------------------------------------|------------------------------------------------------------------------------------------|
| [Botania](https://modrinth.com/mod/botania)      | Tech themed around magic, flowers, and nature.                                           |
| [Create](https://modrinth.com/mod/create-fabric) | Tech themed around clockwork and large mechanisms.                                       |
| [Yttr](https://modrinth.com/mod/yttr)            | Tech, magic, and world themed around the void, mistakes, and a union of past and future. |

<details>
<summary>Click to show all Modded mods</summary>

```
Mods:
  -Amplified Nether (replaced by yttr)
  Botania
  Create Fabric
  Lib39
  Patchouli
  Create: Slice and Dice
  Yttr
```

</details>

Below this section continues the Tinkerer's Quilt Plus description.

## Vanilla Ice [Vanilla+ Content]

Tinkerer's Quilt Plus is a **Vanilla+ Modpack** - It adds a bit of content to items, blocks, and worlds, but doesn't add any elaborate new systems.

If you're looking for the original vanilla-compatible pack, that's [Tinkerer's Quilt](https://modrinth.com/modpack/tinkerers-quilt).

The content added is a slight expansion on exploration, self-defense, and homesteading. In total, it bumps the quest count to 193.


| Relevant Mod                                                                   | Summary                                                     |
|--------------------------------------------------------------------------------|-------------------------------------------------------------|
| [Aurora's Decorations](https://modrinth.com/mod/aurorasdecorations)            | Adds decorations, including two wood types and a new biome. |
| [Antique Atlas](https://modrinth.com/mod/antique-atlas)                        | Press M to open a full dimension map.                       |
| 🛠️ [Antique Fwaystones](https://modrinth.com/mod/antique-fwaystones)          | Adds global atlas markers for discovered waystones.         |
| [Campanion](https://modrinth.com/mod/campanion)                                | Adds camping-themed tools and building materials.           |
| [Consistency+](https://modrinth.com/mod/consistencyplus)                       | Adds missing block type variations for many blocks.         |
| [Earth2Java](https://modrinth.com/mod/earth2java)                              | Adds cosmetic passive entity variants.                      |
| [Fabric Seasons](https://modrinth.com/mod/fabric-seasons)                      | Adds crop-affecting and visual seasons to the world.        |
| [Farmer's Delight](https://modrinth.com/mod/farmers-delight-fabric)            | Adds many new crops and cooking methods.                    |
| [Fabric Waystones](https://modrinth.com/mod/fwaystones)                        | Adds a fast travel system of standing stones.               |
| [Lovely Snails](https://modrinth.com/mod/lovely-snails)                        | Adds tameable and rideable snails to the world.             |
| [Repurposed Structures](https://modrinth.com/mod/repurposed-structures-fabric) | Adds new village variants to the world.                     |
| [Sandwichable](https://modrinth.com/mod/sandwichable)                          | Allows making arbitrary sandwiches out of foods.            |
| [YUNGs Better Mods](https://modrinth.com/user/YUNGNICKYOUNG)                   | Overhauls various vanilla structures for intricacy.         |
| [Ecotones](https://modrinth.com/user/ecotones)                                 | Adds a new optional world type with flowing terrain. (Disclaimer: Not all quests are completable in the ecotones world type!) |


<details>
<summary>Click to show all Plus mods</summary>

```
Mods:
[Jar/Fork] Aileron
Amplified Nether
[Jar/Fork] Antique Atlas
Antique Fwaystones
Architecture Extensions
Aurora's Decorations
Bovines and Buttercups (Mooblooms)
Campanion
Chalk (Fabric)
Consistency+
Dawn API
Earth2Java
Ecotones
EMICompat
Fabric Seasons: Delight Compat
[Jar/Fork] Fabric Seasons
Farmer's Delight [Fabric]
Fabric Waystones
Geophilic
Halfdoors
Lovely Snails
Meal API
MidnightLib
Nature's Compass
Polaroid Camera
Repurposed Structures - Quilt/Fabric
[Jar/Fork] Sandwichable
Trinkets
Universal Ores
YUNG's API
YUNG's Better Desert Temples
YUNG's Better Dungeons
YUNG's Better Mineshafts
YUNG's Better Nether Fortresses
YUNG's Better Ocean Monuments
YUNG's Better Strongholds
YUNG's Better Witch Huts
```

</details>

Below this section continues the original Tinkerer's Quilt Description.

## A Playable Minecraft Wiki [Vanilla Quests]

Tinkerer's Quilt adds **166 quests** that help players explore minecraft.<br/>
From **basic game progression** to **niche mechanics** like mechanisms, building furniture, and **using beds as weapons**.

<details>
<summary>Click to show Quest Previews</summary>

#### The Basics - Overworld, Nether, End<br/>
![Quests: The Basics](https://cdn.modrinth.com/data/vrkQdo9Y/images/9093fe651282b211fb41a564e397ad56f39d15c1.png)

#### Homesteading - Transport, Housing, Farming (Renewables), Workshopping (Mechanisms)
![Quests: Homesteading](https://cdn.modrinth.com/data/vrkQdo9Y/images/5f5b0a4862a17bbce88b14ff54823a0619c4840c.png)

#### Self-Defense - Armaments, Challenges, Foes
![Quests: Self-Defense](https://cdn.modrinth.com/data/vrkQdo9Y/images/c6da473727616e599ba5e2b00bccba73e52e6bd9.png)

#### Exploration - Wanderer, Sightseer, Naturalist
![Quests: Exploration](https://cdn.modrinth.com/data/vrkQdo9Y/images/d53599b09dd8b70e7dbbee1d36fc7f2da2fe5dc8.png)

</details>


| Relevant Mod                                                                              | Summary                                          |
|-------------------------------------------------------------------------------------------|--------------------------------------------------|
| 🛠️ [Crunchy Crunchy Advancements](https://modrinth.com/mod/crunchy-crunchy-advancements) | Removes advancements so you can focus on quests. |

## My Other Modpack Description Is A Quest [In-Game Documentation]

Tinkerer's Quilt has all the information you need to know about the pack **in-game**.

No wiki, nothing - you **don't even need to read this whole description**.

Most of the information is available in the **Introduction Quest Category**:
![Quests: Introduction](https://cdn.modrinth.com/data/vrkQdo9Y/images/54e0e54c3c6097660e3810a7d7149ccd503c6af1.png)


| Relevant Mod                                             | Summary                                                             |
|----------------------------------------------------------|---------------------------------------------------------------------|
| [EMI](https://modrinth.com/mod/emi)                      | An advanced recipe book for learning how to craft and use any item. |
| [What The Hell Is That?](https://modrinth.com/mod/wthit) | Hold alt to see information on the block or mob you're looking at.  |
| 🛠️ [PicoHUD](https://modrinth.com/mod/picohud)          | Hold alt to see your location, direction, and the in-game time.     |

## Smooth As Butter [Game Rebalancing / QoL]

Tinkerer's Quilt focuses rebalancing on making the game accessible to more people, but also taking the sting out of **frustrating parts of vanilla**.

It's not just about making the game easier - with annoyances alleviated in death, movement, and equipment mechanics, more experienced players can **crank the difficulty** and have a blast!

| Relevant Mod                                                                   | Summary                                                                            |
|--------------------------------------------------------------------------------|------------------------------------------------------------------------------------|
| [Apathy](https://modrinth.com/mod/apathy)                                      | Mobs are neutral on easy difficulty, and will only attack you if provoked.         |
| [You're In Grave Danger](https://modrinth.com/mod/yigd)                        | Leaves a grave on death with your items and XP that won't de-spawn.                |
| 🛠️ [Tinkerer's Smithing](https://modrinth.com/mod/tinkerers-smithing)         | Repair, upgrade, and maintain your favourite tools.                                |
| [Project: Save The Pets!](https://modrinth.com/mod/projectsavethepets)         | Prevents pet harm and adds a pet revival system.                                   |
| [Horse Buff](https://modrinth.com/mod/horsebuff)                               | Improves and protects horses, making them easier to take out on adventures.        |
| [Reacharound](https://modrinth.com/mod/reacharound)                            | Build midair bridges without sneaking, and even downwards stairs.                  |
| [Fabrication](https://modrinth.com/mod/fabrication)                            | A multitude of minor improvements, like automatic block pickup and crawling.       |
| [Noteable](https://modrinth.com/mod/noteable)                                  | Allows players to take in-game notes about their plans, locations to revisit, etc. |
| 🛠️ [Portable Crafting](https://modrinth.com/mod/portable-crafting-standalone) | Open the crafting table from your inventory.                                       |



## Three Demons, Two Lizards, a Bug, and a Fish all walk into a Bar [Character Customization]

Tinkerer's Quilt **Encourages player expression** with better skins, origins, and more ways to build.

| Relevant Mod                                                      | Summary                                                                                                                                                         |
|-------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Ears](https://ears.unascribed.com/)                              | Skins with tails, horns, ears, claws, wings and more!                                                                                                           |
| [Fabric Tailor](https://modrinth.com/mod/fabrictailor)            | Swap your skin in-game.                                                                                                                                         |
| 🛠️ [Drogstyle](https://modrinth.com/mod/drogtor)                 | Change player name, color, and bio (e.g. pronouns) any time.                                                                                                    |
| 🛠️ [Switchy](https://modrinth.com/mod/switchy)                   | Swap between player profiles with names, skins, or even inventories!                                                                                            |
| [Origins](https://modrinth.com/mod/origins)                       | Choose an _Origin_, with abilities and limitations.                                                                                                             |
| 🛠️ [Origins Minus](https://modrinth.com/mod/origins-minus)       | Simple origins with fun abilities!                                                                                                                              |
| 🛠️ [Tinkerer's Statures](https://modrinth.com/mod/origins-minus) | Choose your height and size with origins!                                                                                                                       |
| [Presence Footsteps](https://modrinth.com/mod/presence-footsteps) | Footstep audio redone - allows changing footstep gait for quadrupeds/etc.                                                                                       |
| [Styled Chat](https://modrinth.com/mod/styled-chat)               | Allows emoji in chat, including discord [twemoji](https://github.com/twitter/twemoji), [Mothcharm's nice emoji](https://mothcharm.carrd.co/#emoji), and extras. |
| [Fabrication](https://modrinth.com/mod/fabrication)               | A huge tweak mod, includes hiding armor and improvements for minecart networks.                                                                                 |
| [Carpet](https://github.com/gnembon/fabric-carpet)                | Many server tweaks, including mechanism and renewable expansions.                                                                                               |

## Pretty Pretty [Visuals]

Tinkerer's Quilt is just as opinionated about visuals as it is about everything else - it includes various visual tweaks and improvements for clarity, performance, and often just plain aesthetic!

In addition to the below, Tinkerer's Quilt has support for the majority of optifine-format resource packs, including things like connected textures and custom skyboxes, thanks to [optifine alternative mods](https://lambdaurora.dev/optifine_alternatives/) made by the community.

| Relevant Mod                                                                       | Summary                                                                                                                                                                                                                                    |
|------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Vanilla Tweaks](https://vanillatweaks.net/)                                       | Decaying tools, less obtrusive shields and fire, and minor texture changes.                                                                                                                                                                |
| [Sodium](https://modrinth.com/mod/sodium)                                          | Client go fast.                                                                                                                                                                                                                            |
| [Iris](https://modrinth.com/mod/iris)                                              | Allows using shaders like [Complementary](https://modrinth.com/shader/complementary-shaders-v4), [Prismarine](https://github.com/Septonious/Prismarine-Shader), and [Super Duper Vanilla](https://modrinth.com/shader/super-duper-vanilla) |
| [Xali's Enchanted Books](https://modrinth.com/resourcepack/xalis-enchanted-books)  | Unique textures for enchanted books.                                                                                                                                                                                                       |
| [Sully's Peeves](https://www.curseforge.com/minecraft/texture-packs/sullys-peeves) | Texture improvements for many everyday blocks and items.                                                                                                                                                                                   |

---

# More Info

## Installation

### Stable (Modrinth)

Use your modrinth-supporting launcher of choice (e.g. [Prism](https://prismlauncher.org/)) - follow [modrinth instructions](https://docs.modrinth.com/docs/modpacks/playing_modpacks/#multimc-and-prism-launcher), then **manually download the mods** listed in the [changelog](https://modrinth.com/modpack/tinkerers-quilt/changelog) for your version.

### Edge (Unsup)

This pack version auto-updates every launch. Slightly higher chance of experiencing pack-jank.

- Install [Prism Launcher](https://prismlauncher.org/) and [Java 17](https://adoptium.net/temurin/releases/) (on Windows? choose `Windows-x64-JRE-17-.msi`)
- Download Tinkerer's Quilt from the [downloads page](https://sisby-folk.github.io/tinkerers-quilt/).
- Drag and drop onto an open Prism window, press OK, and double click it to play!
- Whenever the pack updates, you'll be prompted on launch for if you'd like to update.
- When an update contains config changes, you'll often be prompted to allow the update overwrite your own. 
  - Clicking "yes to all" will allow the update to change these files universally.
  - If you're a more experienced user, you can manually reject configs you've set up how you like.
- Note that **deleting a file is no longer sufficient for it to be redownloaded**. To force the pack to "reverify" all pack files next launch (like steam), delete `unsup_state.json` from the instance folder.

#### Server

- Download the server from the [downloads page](https://sisby-folk.github.io/tinkerers-quilt/).
- Run the included Quilt Installer, selecting your minecraft version and the latest non-beta loader version.
- The provided `Start` script will both update and run the server via the JVM argument.

## Full Modlist

<details>
<summary>Click to show List</summary>

```
Resource Packs:
    Axolotl Bucket Variants
    Sully's Peeves
    Vanilla Tweaks
    Varied Connected Bookshelves
    xali's Enchanted Books

Mods:
    Advancements Debug
    Ambient Environment
    Animatica
    Apathy
    AppleSkin
    Architectury API
    bad packets
    Balm
    BedrockWaters
    Better End Sky
    Boring Default Game Rules
    Cardinal Ice Boats
    Carpet Extra
    Carpet-Fixes
    Carpet
    Charmonium
    ChickensShed
    Chunks fade in
    CIT Resewn
    ClickThrough
    Client Tweaks
    Cloth Config API
    Colormatic
    Continuity
    Couplings
    Coyote Time
    Crowmap
    Crunchy Crunchy Advancements
    Drogstyle
    [Jar/Fork] Dyable Fishing Lines
    Dynamic FPS
    Ears (+ Snouts/Muzzles, Tails, Horns, Wings, and More)
    EasierEnchanting
    Effective
    Enhanced Block Entities
    EMI
    EMI Trades
    Enhanced Attack Indicator
    Exordium
    Window Title Changer
    Fabrication
    Fabric Shield Lib
    Fabric Tailor
    Falling Leaves
    FastOpenLinksAndFolders
    Feed the Bees
    FerriteCore
    [Fork/Jar] Fishing Real
    FTB Library (Fabric)
    FTB Quests (Fabric)
    FTB Teams (Fabric)
    Horse Buff
    Idwtialsimmoedm
    ImmediatelyFast
    Indium
    [Jar/Fork] Inventory Tabs
    Inspecio
    Iris Shaders
    Item Filters
    Item Model Fix
    JamLib
    Kaffee's Dual Ride
    Keep My Hand
    Kiwi 🥝
    Krypton
    LambdaBetterGrass
    LambDynamicLights
    LazyDFU
    Leaves Us In Peace
    Lithium
    Luna Slimes
    Main Menu Credits
    LAN World Plug-n-Play (mcwifipnp)
    Mod Menu
    More Culling
    Mouse Wheelie
    Music Notification
    Not Enough Animations
    Noteable
    Ok Zoomer
    [Jar/Fork] Origins
    Origins Minus
    oωo (owo-lib)
    Pehkui
    Pettable
    PicoHUD
    Portable Crafting
    Presence Footsteps
    Project: Save the Pets!
    QDResLoader
    Quilt Kotlin Libraries (QKL)
    Quilt Standard Libraries
    Quilt Loading Screen
    Reacharound
    Reese's Sodium Options
    RightClickHarvest
    Simple Durability Tooltip
    Simple Fog Control
    Skip Transitions
    Snow! Real Magic! ⛄
    Snow Under Trees (Fabric)
    Sodium Extra
    Sodium
    Starlight (Fabric)
    Status Effect Timer
    Styled Chat
    Styled Nicknames
    Styled Player List
    SwingThrough
    Switchy Inventories
    Switchy Proxy
    Switchy Resource Packs
    Switchy Teleport
    Switchy
    SwitchyKit
    Tax Free Levels
    Tinkerer's Smithing
    Tinkerer's Statures
    Loading Screen Tips
    ToolTipFix
    Totem Anywhere
    VehicleFix
    Visuality
    WTHIT Plugins
    WTHIT
    You're in Grave Danger

Recommended Shader Packs:
    BSL Shaders
    Builder's QOL Shaders
    Complementary Reimagined
    Complementary Shaders v4
    Prismarine Shaders
    Rethinking Voxels
    Sildurs Enhanced Default Shaders
    Solas Shader
    Super Duper Vanilla
```

If you can't get by without a map mod, try our [fork of Antique Atlas](https://github.com/sisby-folk/AntiqueAtlas/releases/tag/v8.0.1%2Bitemless) without items.

</details>

## Afterword

All present minecraft modding is built on a decade of work from other people (and non-people, we don't discriminate).

This modpack started in January 2021 as a skyrim-style vanilla+ "modding guide" - and only exists in the form it has today thanks to much help from the community!

### Special Thanks

[Emi](https://github.com/emilyploszaj) ([EMI](https://modrinth.com/mod/emi))<br/>
[Una](https://github.com/unascribed) ([Fabrication](https://github.com/unascribed/Fabrication), [Ears](https://modrinth.com/mod/ears), [Drogtor](https://github.com/unascribed/Drogtor/), [Yttr](https://modrinth.com/mod/yttr), [Unsup](https://git.sleeping.town/unascribed/unsup))<br/>
[Quat](https://github.com/quat1024) ([Apathy](https://modrinth.com/mod/apathy))<br/>
[Comp500](https://github.com/comp500) ([Packwiz](https://github.com/packwiz/packwiz))<br/>
[LambdAurora](https://github.com/LambdAurora) ([QSL](https://modrinth.com/mod/qsl), [LambdaBetterGrass](https://modrinth.com/mod/lambdabettergrass), [LambDynamicLights](https://modrinth.com/mod/lambdynamiclights))<br/>
[Patbox](https://github.com/Patbox) ([Styled Nicknames](https://modrinth.com/mod/styled-nicknames), [Styled Chat](https://modrinth.com/mod/styled-chat))<br/>
[Vazkii](https://github.com/Vazkii) ([Bliss](https://www.curseforge.com/minecraft/modpacks/bliss))<br/>
[Infinidoge](https://github.com/Infinidoge) ([Simple Durability Tooltip](https://modrinth.com/mod/simple-durability-tooltip))<br/>
Mod contributors and collaborators - [Ampflower](https://github.com/Ampflower), [Pug](https://github.com/MerchantPug), [LemmaEOF](https://github.com/LemmaEOF), [Silver](https://github.com/SilverAndro), [Garden System](https://github.com/HestiMae), [Kat](https://github.com/Bluberry-Kat/), Aquaholic, [Octal](https://modrinth.com/user/Octal), Ami, [Leo](https://github.com/leo60228), Janet, [Jasmine](https://github.com/jaskarth), and also we have walnut memory so probably like a lot of other people lets be honest - [Falkreon](https://github.com/falkreon) probably? you get the picture.<br/>
More generally - unascribed's guild and its inhabitants<br/>
The queer pluralfolk who initially tested the pack in early 2022 (you know who you are)<br/>
[Bleu](https://twitter.com/bleudrawsthings) (for drawing the original Kapesi bust in the icon/banner art)<br/>
[DuppyPuff](https://www.tumblr.com/duppypuff) (for drawing kapesi used in the new icon/banner art)<br/>
The many, many other folks who've provided development support and feedback for us and our modding ventures - this project wouldn't be possible without you!

#### License Credits

```
Vanilla Tweaks: https://vanillatweaks.net/
OriginsPlus and Lepus: Dan's Other Clone https://www.youtube.com/c/DansOtherClone
Effective: https://modrinth.com/mod/effective
```

---

<center><b>Packs:</b> <i>Tinkerer's Quilt</i> (<a href="https://modrinth.com/modpack/tinkerers-silk">Silk</a>) - <a href="https://modrinth.com/modpack/switchy-pack">Switchy Pack</a></center>
<center><b>Mods:</b> <a href="https://modrinth.com/mod/switchy">Switchy</a> - <a href="https://modrinth.com/mod/origins-minus">Origins Minus</a> (<a href="https://modrinth.com/mod/tinkerers-statures">Statures</a>) - <a href="https://modrinth.com/mod/tinkerers-smithing">Tinkerer's Smithing</a></center>

---
